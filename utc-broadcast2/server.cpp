#include <iostream>

#include <sys/types.h>
#include <sys/socket.h>

#include <netdb.h>
#include <string.h>


#include "unp.h"


#define MAXLINE 5


void dg_echo(int sockfd, struct sockaddr *pcliaddr, socklen_t clilen)
{
	int n;
	socklen_t len;
	char mesg[MAXLINE];

	for(;;) {
		len = clilen;
		n = recvfrom(sockfd, mesg, MAXLINE, 0, pcliaddr, &len);
        std::cout << "recvd " << n << " -> " << sock_ntop_host(pcliaddr, len) << std::endl;
        
		n = sendto(sockfd, mesg, n, 0, pcliaddr, len);
        std::cout << "sended " << n << std::endl;
        std::cout << "" << std::endl;
	}
} 


int main(int argc, char **argv)
{
	int sockfd;
	struct sockaddr_in servaddr, cliaddr;

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	memset(&servaddr, 0, sizeof(servaddr));

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(12345);
	
    const int on = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on));
    
	bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));
	
    std::cout << "sockfd " << sockfd << std::endl;

	dg_echo(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr));

    return 0;
}
