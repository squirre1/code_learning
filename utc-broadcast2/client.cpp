#include <iostream>

#include <arpa/inet.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netdb.h>
#include <string.h>


#define MAXLINE 1000
#define SERV_PORT 12345


void dg_cli(FILE *fp, int sockfd, struct sockaddr *pservaddr, socklen_t servlen)
{
	int n;
	char sendline[MAXLINE], recvline[MAXLINE+1];
    
	while(fgets(sendline, MAXLINE, fp) != NULL) {
		sendto(sockfd, sendline, strlen(sendline), 0, pservaddr, servlen);
        
		n = recvfrom(sockfd, recvline, MAXLINE, 0, NULL, NULL);
        recvline[n] = '\0';
        fputs(recvline, stdout);
	}
} 


int main(int argc, char **argv)
{
	int sockfd;
	struct sockaddr_in servaddr;

    if (argc != 2) {
        std::cerr << "argc != 2" << std::endl;
        return -1;
    }
    
	memset(&servaddr, 0, sizeof(servaddr));
    
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(SERV_PORT);
    inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
    
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    
    std::cout << "sockfd " << sockfd << std::endl;

	dg_cli(stdin, sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

    return 0;
}
