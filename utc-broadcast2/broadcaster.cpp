#include <iostream>

#include <arpa/inet.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netdb.h>
#include <string.h>


#define MAXLINE 1000
#define SERV_PORT 12345


void dg_cli(FILE *fp, int sockfd, struct sockaddr *pservaddr, socklen_t servlen)
{
	int n;
    
    char sendline[] = "foobar";
    
    

    struct sockaddr *preply_addr;
    preply_addr = (struct sockaddr *) malloc(servlen);

    const int on = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST, &on, sizeof(on));
    
	// while(fgets(sendline, MAXLINE, fp) != NULL) {
	// 	sendto(sockfd, sendline, strlen(sendline), 0, pservaddr, servlen);
        
	// 	n = recvfrom(sockfd, recvline, MAXLINE, 0, NULL, NULL);
    //     recvline[n] = '\0';
    //     fputs(recvline, stdout);
	// }

    while(1) {
        sleep(5);
        n = sendto(sockfd, sendline, strlen(sendline), 0, pservaddr, servlen);
        std::cout << "broadcasted " << n << std::endl;
            
    }

} 


int main(int argc, char **argv)
{
	int sockfd;
	struct sockaddr_in servaddr;

    if (argc != 2) {
        std::cerr << "argc != 2" << std::endl;
        return -1;
    }
    
	memset(&servaddr, 0, sizeof(servaddr));
    
	servaddr.sin_family = AF_INET;
	servaddr.sin_port = htons(SERV_PORT);
    servaddr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    
    // inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
    
    
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    
    std::cout << "sockfd " << sockfd << std::endl;

	dg_cli(stdin, sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

    return 0;
}
