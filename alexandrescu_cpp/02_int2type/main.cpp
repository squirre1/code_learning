#include <iostream>
#include <cstdlib>
#include <typeinfo>

template<int v>
struct Int2Type
{
	enum {value = v };
};

struct Foo {
	int x;
};

struct Bar {
	double y;
};


template<class T, bool IsPolymorph>
class NiftyContanerNotWorking
{
public:
	void Do(T *obj) {
		std::cout << "Do" << std::endl;
		if (IsPolymorph) {
			std::cout << obj->x << std::endl;
		}
		else {
			std::cout << obj->y << std::endl;
		}
	}
};


template<class T, bool IsPolymorph>
class NiftyContaner
{
private:
	void Do(T *obj, Int2Type<true>) {
		std::cout << "Do <" << typeid(T).name() << "> x=" << obj->x << std::endl;
	}
	void Do(T *obj, Int2Type<false>) {
		std::cout << "Do <" << typeid(T).name() << "> y=" << obj->y << std::endl;
	}
public:
	void Do(T *obj) {
		std::cout << "DO " << IsPolymorph << " " << std::endl;
		Do(obj, Int2Type<IsPolymorph>());
	}
//DO 1 8Int2TypeILi1EE
//DO 1 F8Int2TypeILi1EEvE
};


int main()
{
	std::cout << Int2Type<0>::value << " " << typeid(Int2Type<0>).name() << std::endl;
	std::cout << Int2Type<1>::value << " " << typeid(Int2Type<1>).name() << std::endl;

	Foo foo; foo.x = 7;
	NiftyContaner<Foo, true> f;
	f.Do(&foo);
	
	Bar bar; bar.y = 8.5;
	NiftyContaner<Bar, false> b;
	b.Do(&bar);
	
	return EXIT_SUCCESS;
}
