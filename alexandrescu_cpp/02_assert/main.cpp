#include <iostream>
#include <cstdlib>
#include <cassert>

template<class To, class From>
To safe_static_cast(From from)
{
	assert(sizeof(From) <= sizeof (To));
	return static_cast<To>(from);
}

#define STATIC_CHECK(expr) { char unnamed[(expr) ? 1 : 0]; }


template<class To, class From>
To safe_static_cast2(From from)
{
	STATIC_CHECK(sizeof(From) <= sizeof (To));
	return static_cast<To>(from);
}


template<bool> struct CompileTimeError;
template<> struct CompileTimeError<true>  {};


#define STATIC_CHECK3(expr) (CompileTimeError<(expr) != 0>())


template<class To, class From>
To safe_static_cast3(From from)
{
	STATIC_CHECK3(sizeof(From) <= sizeof (To));
	return static_cast<To>(from);
}



template<bool> struct CompileTimeChecker
{
	CompileTimeChecker(...);
};
template<> struct CompileTimeChecker<false>  {};

#define STATIC_CHECK4(expr, msg) \
  {\
    class ERROR_##msg {}; \
	(void)sizeof((CompileTimeChecker<(expr) != 0> (ERROR_##msg())));	\
  }

template<class To, class From>
To safe_static_cast4(From from)
{
	STATIC_CHECK4(sizeof(From) <= sizeof (To), Dest_type_too_narrow);
	return static_cast<To>(from);
}


// template<class To, class From>
// To safe_static_cast4(From from)
// {
// 	{ 
// 		class ERROR_Dest_type_too_narrow {}; 
// 		(void)sizeof(  (CompileTimeChecker<(sizeof(From) <= sizeof (To)) != 0> ( ERROR_Dest_type_too_narrow() ) ));
// 	};
// 	return static_cast<To>(from);
// }


int main()
{
	std::cout << "Hello, world" << std::endl;
	
	char c = 49;
	int i = static_cast<int>(c);
	std::cout << "static_cast<int>=" << i << std::endl;

	int is = safe_static_cast<int>(c);
	std::cout << "safe_static_cast<int>=" <<is << std::endl;

	int is4 = safe_static_cast4<int>(c);
	std::cout << "safe_static_cast4<int>=" <<is4 << std::endl;


	int i2 = 72300;
	char c2 = static_cast<char>(i2);
	std::cout << "static_cast<char>=" << c2 << std::endl;

	char c2s = safe_static_cast<char>(i2);
	std::cout << "safe_static_cast<char>=" << c2s << std::endl;

	// char c2s2 = safe_static_cast2<char>(i2);
	// std::cout << "safe_static_cast2<char>=" << c2s2 << std::endl;
	
	// char c2s2 = safe_static_cast3<char>(i2);
	// std::cout << "safe_static_cast3<char>=" << c2s2 << std::endl;
	
	char c2s2 = safe_static_cast4<char>(i2);
	std::cout << "safe_static_cast4<char>=" << c2s2 << std::endl;
	
	
	return EXIT_SUCCESS;
}
