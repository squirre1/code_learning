#include <iostream>
#include <cstdlib>
#include <typeinfo>

struct Widget {
	int x;
	int y;
	Widget *Clone() { return new Widget(); }
};

struct Gadget {
	int x;
	int y;
	int z;
	Gadget *Clone() { return new Gadget(); }
};

template<class T>
struct MallocCreator {
	T *Create() {
		std::cout << "MallocCreator <" << typeid(T).name() << ">" << std::endl;
		void *buf = std::malloc(sizeof(T));
		if (!buf) 
			return 0;
		return new (buf) T; // placement new
	}
};

template<class T>
struct OpNewCreator {
	T *Create() {
		std::cout << "OpNewCreator <" << typeid(T).name() << ">" << std::endl;
		return new T();
	}
};

template<class T>
struct PrototypeCreator {
	PrototypeCreator(T *pObj = 0) : pPrototype_(pObj) {}
	T *Create() {
		std::cout << "PrototypeCreator <" << typeid(T).name() << ">" << std::endl;
		return pPrototype_ ? pPrototype_->Clone() : 0;
	}
	T *GetPrototype() { return pPrototype_; }
	void SetPrototype(T *pObj) { pPrototype_ = pObj; }
private:
	T *pPrototype_;
};




template<class CreationPolicy>
class WidgetManager : public CreationPolicy
{
// public:
// 	WidgetManager() {}
};

// template template parameters
template<template <class Created> class CreationPolicy = MallocCreator>
class WidgetManagerTT : public CreationPolicy<Widget>
{
public:
	template<typename T>
	T *AnotherType() {
		return CreationPolicy<T>().Create();
	}
};

template<class T = int>
struct MallocCreatorIntDefault {
	T *Create() {
		std::cout << "MallocCreatorIntDefault" << std::endl;
		void *buf = std::malloc(sizeof(T));
		if (!buf) 
			return 0;
		return new (buf) T; // placement new
	}
};




typedef WidgetManager< MallocCreator<Widget> > MyMallocWidgetManager;

typedef WidgetManager< OpNewCreator<float> > MyFloatOpManager;

typedef WidgetManager< PrototypeCreator<Widget> > MyProtoWidgetManager;

typedef WidgetManagerTT< OpNewCreator> MyWidgetOpManagerTT;




int main()
{
	{
		MyProtoWidgetManager wman;
		wman.SetPrototype(new Widget);
		std::cout << "Creating Widget with PrototypeCreator" << std::endl;
		Widget *res = wman.Create();
		res->x = 1;
		std::cout << "res->x = "  << res->x << std::endl;
		std::cout << std::endl;
		delete res;
	}

	{
		MyMallocWidgetManager wman;
		std::cout << "Creating Widget with MallocCreator" << std::endl;
		Widget *res = wman.Create();
		res->x = 2;
		std::cout << "res->x = "  << res->x << std::endl;
		std::cout << std::endl;
		delete res;
	}

	
	{
		MyFloatOpManager wman;
		std::cout << "Creating float with OpNewCreator" << std::endl;
		float *res = wman.Create();
		*res = 2.0;
		std::cout << "res->x = "  << *res << std::endl;
		std::cout << std::endl;
		delete res;
	}


	{
		MyWidgetOpManagerTT wman;
		std::cout << "Creating Widget with OpNewCreator over TTP with typedef" << std::endl;
		Widget *res = wman.Create();
		res->x = 3;
		std::cout << "res->x = "  << res->x << std::endl;
		std::cout << std::endl;
		delete res;
	}

	{
		WidgetManagerTT <OpNewCreator> wman;
		std::cout << "Creating Widget with OpNewCreator over TTP with typedef" << std::endl;
		Widget *res = wman.Create();
		res->x = 4;
		std::cout << "res->x = "  << res->x << std::endl;
		std::cout << std::endl;
		delete res;
	}

	{
		WidgetManagerTT<>  wman; //default arg
		std::cout << "Creating Widget with DEFAULT_ARG over TTP with typedef" << std::endl;
		Widget *res = wman.Create();
		res->x = 5;
		std::cout << "res->x = "  << res->x << std::endl;
		std::cout << std::endl;
		delete res;

		std::cout << "Creating AnotherType(int) with OpNewCreator over TTP with typedef" << std::endl;
		int *res2 = wman.AnotherType<int>();
		*res2 = 8;
		delete res2;

	}

	
	MallocCreator<float> mc;
	MallocCreatorIntDefault<> mc2;
	//MallocCreatorIntDefault mc2; does not compile
	
	return 0;
}
