#include <iostream>

#include <cstdlib>

template<bool> struct CTAssert;

template<> struct CTAssert<true> {};


int main()
{
	CTAssert<1>();
	std::cout << "main" << std::endl;
	return EXIT_SUCCESS;
}


// http://www.ksvanhorn.com/Articles/ctassert.html
// ?????
