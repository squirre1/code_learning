#include <iostream>
#include <cstdlib>
#include <typeinfo>

struct Widget {
	Widget(float pos, int arg) : pos_(pos), arg_(arg) {}
	float pos_;
	int arg_;
	void print() {
		std::cout << "Widget(" << this << ") pos=" << pos_ << " arg=" << arg_ << std::endl;
	}
	
};

struct Box : public Widget {
	Box(bool val) : Widget (0.5, -2), val_(val) {}
	bool val_;
	
	void print() {
		std::cout << "Box(" << this << ") " << (val_ ? "true" : "false") << " pos=" << pos_ << " arg=" << arg_ << std::endl;
	}
};


// Ok, but not for widget
// template<class T, class U>
// T* Create(const U&arg)
// {
// 	return new T(arg);
// }

// not allowed partial function specializaion
// template<class U>
// Widget* Create<Widget, U>(const U&arg)
// {
// 	return new T(arg, -1);
// }



template<class T, class U>
T* Create(const U&arg)
{
	return new T(arg);
}





int main()
{
	Widget w(0.6, -4);
	w.print();
	
	Box b(true);
	b.print();

	std::cout << "=Create=" << std::endl;
	
	Box *b2 = Create<Box>(true);
	b2->print();
	delete b2;

	Widget *w2 = Create<Widget>(4);
	w2->print();
	delete w2;

	// std::cout << Int2Type<0>::value << " " << typeid(Int2Type<0>).name() << std::endl;
	// std::cout << Int2Type<1>::value << " " << typeid(Int2Type<1>).name() << std::endl;
	
	return EXIT_SUCCESS;
}
