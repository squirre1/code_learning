
3.18. /tmp : Temporary files
https://refspecs.linuxfoundation.org/FHS_3.0/fhs/ch03s18.html
Programs must not assume that any files or directories in /tmp are preserved between invocations of the program.

5.15. /var/tmp : Temporary files preserved between system reboots

The /var/tmp directory is made available for programs that require temporary files or directories that are preserved between system reboots. Therefore, data stored in /var/tmp is more persistent than data in /tmp.

Files and directories located in /var/tmp must not be deleted when the system is booted. Although data stored in /var/tmp is typically deleted in a site-specific manner, it is recommended that deletions occur at a less frequent interval than /tmp.
