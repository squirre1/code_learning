
unique_ptr zero overhead - only hold pointer
Deleter is part of type

unique_ptr<T, DeleterType>

shared_ptr is always `template <typename T> class shared_ptr`
has pointer to control block: [object,ref counter (strong), weak counter, deleter]
std::default_delete



**do not use make_unique if you need a custom deleter, or if you are adopting a raw pointer.**


чуть подробнее бы
void sink( foo, unique_ptr<bar> );
sink( create_foo(), unique_ptr<bar>{new bar{}} );


https://herbsutter.com/2013/05/29/gotw-89-solution-smart-pointers/


auto sp1 = shared_ptr<widget>{ new widget{} };
auto sp2 = sp1;

auto sp1 = make_shared<widget>();
auto sp2 = sp1;
make_shared = Single allocation + move


Johnny, [27.06.2022 23:54]
https://stackoverflow.com/questions/6876751/differences-between-unique-ptr-and-shared-ptr
 unique_ptr can be a little buggy regarding deleters. shared_ptr will always do "the right thing", as long it was created with make_shared. But if you create a unique_ptr<Derived>, then convert it to unique_ptr<Base>, and if Derived is virtual and Base is not, then the pointer will be deleted through the wrong type and there can be undefined behaviour. This can be fixed with an appropriate deleter-type in the unique_ptr<T, DeleterType>, but the default is to use the riskier version because it is a little more efficient.

Johnny, [27.06.2022 23:55]
It is not copyable, but movable.

Johnny, [27.06.2022 23:55]
But there's another big difference: The shared pointers type is always template <typename T> class shared_ptr;, and this is despite the fact that you can initialize it with custom deleters and with custom allocators. The deleter and allocator are tracked using type erasure and virtual function dispatch, which adds to the internal weight of the class, but has the enormous advantage that different sorts of shared pointers of type T are all compatible, no matter the deletion and allocation details. Thus they truly express the concept of "shared responsibility for T" without burdening the consumer with the details!

Johnny, [27.06.2022 23:59]
shared_ptr type-erases the deleter, i.e. users of shared_ptr don't have to know what type the deleter has. This has a run-time cost (allocation, dereference), so it isn't performed for unique_ptr (which is overhead-free). E.g. see stackoverflow.com/q/6324694/420683

Johnny, [28.06.2022 00:01]
https://stackoverflow.com/questions/6324694/type-erasure-in-c-how-boostshared-ptr-and-boostfunction-work

Johnny, [28.06.2022 00:02]
https://geidav.wordpress.com/2017/10/29/custom-deleters-for-smart-pointers-in-modern-c/
