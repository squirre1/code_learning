#include <iostream>

#include <memory>

class Foo {
public:
    Foo() { std::cout << "Foo()" << std::endl; }
    ~Foo() { std::cout << "~Foo()" << std::endl; }
};

class Bar : public Foo {
public:
    Bar() { std::cout << "Bar()" << std::endl; }
    ~Bar() { std::cout << "~Bar()" << std::endl; }
};


int main(int argc, char **argv)
{
    std::unique_ptr<Foo> obj = std::make_unique<Bar>();
    return EXIT_SUCCESS;
}
