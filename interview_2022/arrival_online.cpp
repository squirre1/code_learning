#include <iostream>

int example1() {
    const auto l = [i=0]() mutable { return ++i; };
    return l();
}

auto example2() {
    auto l = 
        [i=[] {
                struct S {
                    int v = 5;
                    ~S() { std::cout << "Destructed" << std::endl; }
                };
                return S{};
            }()
            ] {
        return i;
    };

    return l().v;
}

int main() {
    std::cout << "Example1:\n" << example1() << std::endl;
    std::cout << "-----------" << std::endl;
    std::cout << "Example2:\n" << example2() << std::endl;
    return 0;
}
