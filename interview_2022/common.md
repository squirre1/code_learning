# Common

https://isocpp.org/wiki/faq/ctors#empty-parens-in-object-decl

**Is there any difference between List x; and List x();?  A big difference!**

List x;     // Local object named x (of class List)
List x();   // Function named x (that returns a List)


**Delegating constructors**

**Init vector with members with ctor**
std::vector<Fred> a(10, Fred(5,7));  // The 10 Fred objects in std::vector a will be initialized with Fred(5,7)


**Remember: delete p does two things: it calls the destructor and it deallocates the memory.**


Todo:

проверить RVO

стирание типа (делетер shared_ptr)

Pimpl


todo когда-нибудь: 

https://isocpp.org/wiki/faq/ctors#ctor-work-right


Проверить explicit
https://isocpp.org/wiki/faq/ctors#explicit-ctors


https://hackr.io/blog/cpp-interview-questions


finished
https://www.toptal.com/c-plus-plus/interview-questions
