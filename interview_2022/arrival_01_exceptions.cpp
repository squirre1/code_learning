// new return bad_alloc
// in C++11 destructors default to noexcept

// программа не должна вырабатывать исключения во время обработки другого исключения (когда происходит раскрутка стека) — это приведет к аварийному завершению работы программы (фактически вызову abort())


#include <iostream>
#include <exception>
#include <string>

// class Foo {
// public:
//     Foo(std::string _name) : name(_name) { std::cout << "Foo() " << name << std::endl; }
//     Foo() : name("default") { std::cout << "Foo() " << name << std::endl; }
//     ~Foo() { std::cout << "~Foo() " << name << std::endl; }
//     std::string name;
// };

// class Bar {
// public:
//     Bar() {
//         std::cout << "Bar()" << std::endl; 
//         Foo f("first");;
//         // throw std::exception();
//     }
//     void exc() noexcept(true) {
//         // throw std::exception();
//     }
//     ~Bar() { std::cout << "~Bar()" << std::endl; }
//     Foo f;
// };

// class Baz : public Bar {
// public:
//     Baz() {
//         std::cout << "Baz()" << std::endl; 
//         throw std::exception();
//     }
//     ~Baz() { std::cout << "~Baz()" << std::endl; }
//     Foo f;
// };

// int main()
// {
//     try {
//         Bar b;
//         Foo f("second");
//     }
//     catch(...) {
//         std::cout << "yo mf!" << std::endl;
//     }
//     return 0;
// }



/*

// memleak of non-virtual desctructor
#include <iostream>

class b {
public:
    b() {
        std::cout<<"Constructing base \n";
    }
    // virtual
    ~b() {
        std::cout<<"Destructing base \n";
    }
};
class d: public b {
public:
    d() {
        std::cout<<"Constructing derived \n";
    }
    ~d() {
        std::cout<<"Destructing derived \n";
    }
};


int main(void) {
    d *derived = new d();
    b *bptr = derived;
    delete bptr;
    return 0;
}
*/







// struct PrinterBusyException {};

// class Printer {
//     std::string m_location, m_port;
// public:
//     Printer(std::string location,
//             std::string port) 
//         : m_location(location),
//           m_port(port) {
//     }
  
//     bool is_busy() {
//         return true; // for example 
//     }
  
//     ~Printer() noexcept(false) 
//         {
//             if (is_busy()) {
//                 throw PrinterBusyException();
//             }
//         }
// };

// struct SomeException {};

// int main() { 
//     try {
//         Printer printer("localhost", "usb://Kyocera/FS-1020MFP?serial=LDA4322583");
//         // some actions ...
//         throw SomeException();
//     }
//     catch(SomeException exception) {
//         std::cout << "SomeException handled\n";
//     }
//     catch(PrinterBusyException exception) {
//         std::cout << "PrinterBusyException handled\n";
//     }
// }












#include <iostream>

class A {
public:
    A() {}
    ~A() {
        throw 42;
    }
};

int main(int argc, const char * argv[]) {
    try {
        A a;
        throw 32;
    } catch(int a) {
        std::cout << a;
    }
}
