https://hackr.io/blog/cpp-interview-questions

Question: Define Object in C++?

Answer: Object is an instance of the class. An object can have fields, methods, constructors, and related. For example, a bike in real life is an object, but it has various features such as brakes, color, size, design, and others, which are instances of its class.
Question: Define Encapsulation in C++?

Answer: Encapsulation is the process of binding together the data and functions in a class. It is applied to prevent direct access to the data for security reasons. The functions of a class are applied for this purpose. For example, the customers' net banking facility allows only the authorized person with the required login id and password to get access. That is too only for his/her part of the information in the bank data source.
Question: What is an abstraction in C++?

Answer: An abstraction in C++ is hiding the internal implementations and displaying only the required details. For example, when you send an important message through email, at that time, only writing and clicking the send option is used. This outcome is just the success message that is displayed to confirm that your email has been sent. However, the process followed in transferring the data through email is not displayed because it is of no use to you.
Question: Briefly explain the concept of Inheritance in C++.

Answer: C++ allows classes to inherit some of the commonly used state and behavior from other classes. This process is known as inheritance.
