#include <iostream>

class Shape {
  public:
    Shape() { std::cout << "Shape::Shape " << this << std::endl; }
    virtual ~Shape() { std::cout << "Shape::~Shape " << this << std::endl; }
    virtual void draw() { std::cout << "Shape::draw" << std::endl; }
    virtual void move() { std::cout << "Shape::move" << std::endl; }
    // ...
    virtual Shape* clone() const { return new Shape(*this); }
    virtual Shape* create() const { return new Shape(); }
};
class Circle : public Shape {
  public:
    Circle() { std::cout << "Circle::Circle " << this << std::endl; }
    Circle* clone() const { return new Circle(*this); }
    Circle* create() const { return new Circle(); }
    // ...
};



void someCode()
{
    char memory[sizeof(Fred)];
    void* p = memory;
    Fred* f = new(p) Fred();
    // ...
    f->~Fred();   // Explicitly call the destructor for the placed object
}

int main()
{
    Shape *s = new Shape();
    s->move();
    s->~Shape();
    
    new (s) Shape();
    delete s;
    return 0;
}


// Placement syntax
// Placement new
/*

https://en.wikipedia.org/wiki/Placement_syntax


Placement new is used when you do not want operator new to allocate memory (you have pre-allocated it and you want to place the object there), but you do want the object to be constructed. Examples of typical situations where this may be required are:

    You want to create objects in memory shared between two different processes.
    You want objects to be created in non-pageable memory.
    You want to separate memory allocation from construction e.g. in implementing a std::vector<> (see std::vector<>::reserve).
*/


/*

https://isocpp.org/wiki/faq/virtual-functions#virtual-ctors


struct F {  // interface to object creation functions
    virtual A* make_an_A() const = 0;
    virtual B* make_a_B() const = 0;
};

void user(const F& fac)
{
    A* p = fac.make_an_A(); // make an A of the appropriate type
    B* q = fac.make_a_B();  // make a B of the appropriate type
}

struct FX : F {
    A* make_an_A() const { return new AX(); } // AX is derived from A
    B* make_a_B() const { return new BX();  } // BX is derived from B
};
struct FY : F {
    A* make_an_A() const { return new AY(); } // AY is derived from A
    B* make_a_B() const { return new BY();  } // BY is derived from B
};
int main()
{
    FX x;
    FY y;
    user(x);    // this user makes AXs and BXs
    user(y);    // this user makes AYs and BYs
    user(FX()); // this user makes AXs and BXs
    user(FY()); // this user makes AYs and BYs
    
}


*/
