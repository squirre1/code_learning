

Неименованнные каналы
int pipe(int filedes[2]);


именованные каналы
int mkfifo(const char * pathname, mode_t mode);

Очереди сообщений

семафоры как средство синхронизации
int semget(key_t key, int nsems, int flag);

Разделяемая память
int shmget(key_t key, size_t size, int flag);
