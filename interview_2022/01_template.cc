#include <iostream>

using mf = int;

template<typename A, typename B>
bool IsSameClass()
{
    return typeid(A) == typeid(B);
}

template<typename A, typename B>
struct IsSame
{
    static const bool value = false;
};
template<typename A>
struct IsSame<A,A>
{
    static const bool value = true;
};

template<typename A, typename B>
bool IsSameClass2()
{
    return IsSame<A,B>::value;
}

int main2()
{
    std::cout << IsSameClass<int,float>() << std::endl;
    std::cout << IsSameClass<int,int>() << std::endl;
    std::cout << IsSameClass<int,mf>() << std::endl;

    std::cout << std::endl;

    std::cout << IsSameClass2<int,float>() << std::endl;
    std::cout << IsSameClass2<int,int>() << std::endl;
    std::cout << IsSameClass2<int,mf>() << std::endl;
    return EXIT_SUCCESS;
}
