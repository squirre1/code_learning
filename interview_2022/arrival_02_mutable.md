`mutable bool done_;`

friend example - mapper


Mutex!


`Set* self = const_cast<Set*>(this);`


mutable in multithread?


const_cast<>



`const X* p`
means “p points to an X that is const”: the X object can’t be changed via p.

`X* const p`
means “p is a const pointer to an X that is non-const”: you can’t change the pointer p itself, but you can change the X object via p.

`const X* const p`
 means “p is a const pointer to an X that is const”: you can’t change the pointer p itself, nor can you change the X object via p.

`const X& x`
 means x aliases an X object, but you can’t change that X object via x.

`Does “X& const x” make any sense?  `
No, it is nonsense. references are always const

`void inspector() const;`// This member promises NOT to change *this
`void mutator();`        // This member function might change *this

https://isocpp.org/wiki/faq/const-correctness#const-overloading


