#include <iostream>


void foo(int a, int b) {
    std::cout << a << ' ' << b << std::endl;
}
template <typename T1, typename T2> void foo(T1 a, T2 b) = delete;

// template <typename T1, typename T2> void foo(T1 a, T2 b) = delete;


int main()
{
    // foo(2,4);
    // foo(2,4.1);
    return EXIT_SUCCESS;
}
