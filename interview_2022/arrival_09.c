#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int glob = 6; /* глобальная переменная в сегменте инициализированных данных */
char buf[] = "запись в stdout\n";

int main(void)
{
    int var; /* переменная, размещаемая на стеке */
    pid_t pid;
    var = 88;
    if (write(STDOUT_FILENO, buf, sizeof(buf)) != sizeof(buf)) {
        printf("ошибка вызова функции write");
    }
    printf("перед вызовом функции fork\n"); /* мы не сбрасываем */

    if ((pid = fork()) < 0) {
        printf("ошибка вызова функции fork");
    } 
    else if (pid == 0)  {
        // child
        glob++;
        var++;
        sleep(10);
    } 
    else {
        // parent
        /* sleep(10); */
    }
    printf("pid = %d, glob = %d, var = %d\n", getpid(), glob, var);
    exit(0);
}   
