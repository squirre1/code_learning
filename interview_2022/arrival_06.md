
“Resource Acquisition is Initialization” (RAII). I find that “scope-bound resource management”

std::shared_ptr and std::unique_ptr act as an SBRM wrappers for dynamically allocated memory
std::lock_guard, std::unique_lock, and std::shared_lock provide RAII wrappers for managing mutex locking/unlocking

std::string
std::vector
std::array
std::thread
std::mutex


