#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#include "string.h"
#include "intlist.h"


/**
 * single-thread chat based on 'select' technology
 */


int max(int a, int b);
int set_nonblock(int fd);

void init_fd_set(fd_set *, int);
int create_master_socket(void);
int create_slave_socket(struct sockaddr_in *_addr, int _mastersocket);
void close_socket(struct listitem_t *_sock);
char *get_textaddress_from_socket(struct sockaddr_in *_addr);
char *message_login(struct sockaddr_in *_addr);
void socket_send_str(int _sock, char *_str);

int main(int argc, char **argv)
{
	socketlist__init();
	
	int master_socket = create_master_socket();
	
	while(1) {
		fd_set fdset;
		init_fd_set(&fdset, master_socket);
		int maxsocket = max(master_socket, socketlist__max());
		select(maxsocket + 1, &fdset, NULL, NULL, NULL);
		
		struct listitem_t *it;
		for(it = socketlist__begin(); it != NULL;  ) {
			
			if (!(FD_ISSET(it->socket, &fdset))) {
				it = it->next;
				continue;
			}
			
			static char buffer[1024];
			int recv_size = recv(it->socket, buffer, 1024, MSG_NOSIGNAL);
			
			if ((recv_size == 0) && errno != EAGAIN) {



				
				
				struct sockaddr_in SockAddr;
				size_t len = sizeof(struct sockaddr_in);
				getsockname(it->socket, (struct sockaddr *)&SockAddr, &len);
				char *strip = inet_ntoa(SockAddr.sin_addr);
				int port = SockAddr.sin_port;
				


				close_socket(it);
				it = socketlist__cut(it);
				continue;
			}
			
			if (recv_size != 0) {
				send(it->socket, buffer, recv_size, MSG_NOSIGNAL);
			}
			it = it->next;
		} // for
		
		if (FD_ISSET(master_socket, &fdset)) { // new chatuser
			
			struct sockaddr_in addr_struct;
			int slave_socket = create_slave_socket(&addr_struct,
												   master_socket);
			
			socketlist__push(slave_socket);
			
			char *loginmessage = message_login(&addr_struct);
			struct listitem_t *it;
			for(it = socketlist__begin(); it != NULL; it = it->next) {
				socket_send_str(it->socket, loginmessage);
			}
			free(loginmessage);
			
		} // if
		
	} //while
	
	return 0;
}



int max(int a, int b)
{
	return (a > b) ? a : b;
}


// So, we can bind 'accept' to 'select'
int set_nonblock(int fd)
{
	int flags;
#if defined(O_NONBLOCK)
	if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
		flags = 0;
	return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
	flags = 1;
	return ioctl(fd, FIOBIO, &flags);
#endif
}



int create_master_socket(void)
{
	int ms = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	struct sockaddr_in sock_addr;
	sock_addr.sin_family = AF_INET;
	sock_addr.sin_port = htons(12345);
	sock_addr.sin_addr.s_addr = htonl(INADDR_ANY); // 0.0.0.0
	bind(ms, (struct sockaddr *) &sock_addr, sizeof(sock_addr));
	
	set_nonblock(ms);
	
	int optval = 1;
	setsockopt(ms, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
	
	listen(ms,SOMAXCONN);
	
	return ms;
}


void init_fd_set(fd_set *_fdset, int _mastersocket)
{
	FD_ZERO(_fdset);
	FD_SET(_mastersocket, _fdset);

	struct listitem_t *it;
	for(it = socketlist__begin(); it != NULL; it = it->next) {
		FD_SET(it->socket, _fdset);
	}
}


char *get_textaddress_from_socket(struct sockaddr_in *_addr)
{
	static char addrbuff[INET_ADDRSTRLEN];
	int addr_int = _addr->sin_addr.s_addr;
	inet_ntop(AF_INET, &addr_int, addrbuff, INET_ADDRSTRLEN);
	size_t addrbuff_len = strlen(addrbuff);
	
	static char portbuff[6]; // max = "65535\0"
	sprintf(portbuff, "%d", _addr->sin_port);
	size_t portbuff_len = strlen(portbuff);
	
	// "(x.x.x.x:xxxxx)\0"
	size_t strsize = 1 + addrbuff_len + 1
		+ portbuff_len + 1 + 1 ;
	
	char *str = (char *) calloc(1,strsize);
	char *p = str;
	*p = '('; p++;
	memcpy(p, addrbuff, addrbuff_len); p += addrbuff_len;
	*p = ':'; p++;
	memcpy(p, portbuff, portbuff_len); p += portbuff_len;
	*p = ')'; // "\0" added automatically - we use calloc!
	return str;
}


int create_slave_socket(struct sockaddr_in *_addr, int _mastersocket)
{
	socklen_t addr_len = sizeof(*_addr);
	int slavesocket = accept(_mastersocket, (struct sockaddr *) _addr, 
							 &addr_len);
	set_nonblock(slavesocket);
	return slavesocket;
}


void close_socket(struct listitem_t *_sock)
{
	shutdown(_sock->socket, SHUT_RDWR);
	close(_sock->socket);
}

char *message_login(struct sockaddr_in *_addr)
{
	char *straddr = get_textaddress_from_socket(_addr);
	int straddr_len = strlen(straddr);
	
	char *msg_part1 = "[CHAT] ";
	size_t msg_part1_len = strlen(msg_part1);
	
	char *msg_part2 = " user connected\n";
	size_t msg_part2_len = strlen(msg_part2);
	
	size_t msg_res_len = msg_part1_len + straddr_len + msg_part2_len + 1;
	char *msg_res = (char *) calloc(1,msg_res_len);
	char *p = msg_res;
	memcpy(p, msg_part1, msg_part1_len); p += msg_part1_len;
	memcpy(p, straddr, straddr_len); p += straddr_len;
	memcpy(p, msg_part2, msg_part2_len); p += msg_part2_len;
	// "\0" added automatically - we use calloc!
	
	free(straddr);
	return msg_res;
}	
		

void socket_send_str(int _sock, char *_str)
{
	int len = strlen(_str);
	send(_sock, _str, len, MSG_NOSIGNAL);
}




