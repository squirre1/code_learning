#include "intlist.h"

#include <stdlib.h>


static struct listitem_t *global_socket_list;  // default = 0


void socketlist__init(void)
{
	;
}

struct listitem_t *socketlist__construct(void)
{
	struct listitem_t *newitem = 
		(struct listitem_t *) calloc(1, sizeof(struct listitem_t *));
	return newitem;
}

struct listitem_t *socketlist__search(int _sockid)
{
	struct listitem_t *p;
	
	for (p = global_socket_list; p != NULL; p = p->next) {
		if (p->socket == _sockid)
			return p;
	}
	return NULL;
}

int socketlist__max(void)
{
	struct listitem_t *p;
	int max_socket = -1; // we use sockets > 0
	
	for (p = global_socket_list; p != NULL; p = p->next) {
		if (p->socket > max_socket)
			max_socket = p->socket;
	}
	return max_socket;
}


void socketlist__push(int _sockid)
{
	if (socketlist__search(_sockid) != NULL)
		return;
	
	// there is no sockid in list
	struct listitem_t *new_item = socketlist__construct();
	new_item->socket = _sockid;
	new_item->next = global_socket_list;
	global_socket_list = new_item;
}


void socketlist__delete(struct listitem_t *_sock)
{
	struct listitem_t *p, *p2;
	for (p = p2 = global_socket_list; p != NULL; p2 = p, p = p->next) {
		if (p != _sock)
			continue;
		
		// we find it p->socket == _sockid
		if (p == p2) // first element
			global_socket_list = p->next;
		else
			p2->next = p->next;
		free(p);
		return ;
	}
}

struct listitem_t *socketlist__cut(struct listitem_t *_sock)
{
	if (_sock == NULL)
		return NULL;
	
	struct listitem_t *next = _sock->next;
	socketlist__delete(_sock);
	return next;
	
}


struct listitem_t *socketlist__begin(void)
{
	return global_socket_list;
}
