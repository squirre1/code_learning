#pragma once


struct listitem_t {
	struct listitem_t *next;
	int socket;
};

struct listitem_t *socketlist__begin(void);
struct listitem_t *socketlist__search(int _sockid);
struct listitem_t *socketlist__construct(void);
struct listitem_t *socketlist__cut(struct listitem_t *_sock);

void socketlist__delete(struct listitem_t *_sock);
void socketlist__init(void);
void socketlist__push(int _sockid);
int socketlist__max(void);
