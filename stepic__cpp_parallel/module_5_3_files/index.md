

LOCK_EX - эксклюзивная блокировка
LOCK_SH - общая блокировка
LOCK_UN - снятие блокировки

flock(fd, LOCK_*);

flock(fd, LOCK_* | LOCK_NB);


LOCK_NB - неблокирующий режим функции flock


--------------------


if (fd = open(filename, O_WRONLY | O_CREATE | O_EXCL, 0) == -1)
	return false; - не смогли установить блокировку

