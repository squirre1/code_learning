
#include <signal.h>

int main(void)
{
	signal(SIGTERM, SIG_IGN);
	signal(SIGINT, SIG_IGN);

	for(;;) {
	}

	return 0;
}
