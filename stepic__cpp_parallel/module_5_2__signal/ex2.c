#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void)
{
	int child_pid = fork();
	
	if (child_pid != 0) { // parent
		waitpid(child_pid, NULL,0);
		pause();
	}
	else {
	    pause();
	}
	
	return 0;
}
