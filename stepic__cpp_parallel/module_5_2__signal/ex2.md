

Задача на зомби.

Создайте два процесса - родитель и потомок (сохраните их PIDы в /home/box/pid_parent и /home/box/pid_child).

Потомок должен убиваться по SIGTERM. При убийстве потомка не должно остаться процессов-зомби.



Нужно корректно обработать завершение потомка в родительском процессе,
  а именно сделать так, чтобы родитель узнал код возврата потомка. Делается с помощью wait.
