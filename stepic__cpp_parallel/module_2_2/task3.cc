#include <iostream>
#include <string>


class StringPointer {

public:
    StringPointer(std::string *str) : p(str), owner(false) {}
    
    StringPointer(const StringPointer& other) = delete;
    StringPointer& operator=(const StringPointer& other) = delete;
    StringPointer(const StringPointer&& other) = delete;
    StringPointer& operator=(const StringPointer&& other) = delete;
    
    std::string operator*() {
        if (p == nullptr) allocSelf();
        return *p;
    }
    std::string *operator->() {
        if (p == nullptr) allocSelf();
        return p;
    }
    ~StringPointer() {
        if (owner) delete p;
    }

private:
    std::string *p;
    bool owner;
    void allocSelf() {
        owner = true;
        p = new std::string();
    }
};


int main(int argc, char **argv)
{
    std::string s1 = "Hello, world!";

    StringPointer sp1(&s1);
    StringPointer sp2(nullptr);

    std::cout << sp1->length() << std::endl;
    std::cout << *sp1 << std::endl;
    std::cout << sp2->length() << std::endl;
    std::cout << *sp2 << std::endl;

    // //SmartPointer<Foo> sp(new Foo(2,2));
    // SmartPointer<Foo> sp(NULL);
    // std::cout << sp->Sum() << std::endl; 
}
