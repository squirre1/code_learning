#include <iostream>

template<typename T>
class SmartPointer {
private:
    T *pointer;
public:
    SmartPointer(T *p) : pointer(p) {};
    operator T*() { return pointer; }
    T *operator->() { 
        if (!pointer) {
            pointer = new T();
            std::cerr << "Bad pointer" << std::endl;
        }
        return pointer;
    }
    std::ptrdiff_t operator-(SmartPointer<T> p) {
        return pointer - p;
    }
    std::ptrdiff_t operator-(void *p) {
        return pointer - p;
    }
};

class Foo {
private:
    int a,b;
public:
    Foo() : a(0), b(0) {};
    Foo(int aa, int bb) : a(aa), b(bb) {};
    int Sum() { return a + b; }
};


// int main(int argc, char **argv)
// {
//     //SmartPointer<Foo> sp(new Foo(2,2));
//     SmartPointer<Foo> sp(NULL);
//     std::cout << sp->Sum() << std::endl; 
// }
