sudo apt install google-perftools

sudo ldconfig -p | grep tcmalloc

```
./module23 
=== 0.000155
```

```
LD_PRELOAD="libtcmalloc.so.4" ./module23 
=== 0.000002
```

```
LD_PRELOAD="libtcmalloc.so.4" HEAPCHECK=normal ./module23 
WARNING: Perftools heap leak checker is active -- Performance may suffer
=== 0.000517
Have memory regions w/o callers: might report false leaks
Leak check _main_ detected leaks of 400 bytes in 100 objects
The 1 largest leaks:
*** WARNING: Cannot convert addresses to symbols in output below.
*** Reason: Cannot find 'pprof' (is PPROF_PATH set correctly?)
*** If you cannot fix this, try running pprof directly.
Leak of 400 bytes in 100 objects allocated from:
	@ 56372f723767 
	@ 7faa24a31c87 
	@ 56372f72366a 


If the preceding stack traces are not enough to find the leaks, try running THIS shell command:

pprof ./module23 "/tmp/module23.14155._main_-end.heap" --inuse_objects --lines --heapcheck  --edgefraction=1e-10 --nodefraction=1e-10 --gv

If you are still puzzled about why the leaks are there, try rerunning this program with HEAP_CHECK_TEST_POINTER_ALIGNMENT=1 and/or with HEAP_CHECK_MAX_POINTER_OFFSET=-1
If the leak report occurs in a small fraction of runs, try running with TCMALLOC_MAX_FREE_QUEUE_SIZE of few hundred MB or with TCMALLOC_RECLAIM_MEMORY=false, it might help find leaks more re
Exiting with error code (instead of crashing) because of whole-program memory leaks
```



LD_PRELOAD="libjemalloc.so.1" ./module23 
