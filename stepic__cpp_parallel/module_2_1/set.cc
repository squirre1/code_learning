#include <iostream> 

#include <set>
#include <unordered_set>

// struct comparator {
//     bool operator() (const int &a, const int &b) const {
//         return b < a;
//     }
// };

struct hasher {
    size_t operator() (const int &a) const {
        return a % 2;
    }
};

int main()
{
    
    std::unordered_set<int,hasher> s;
    s.insert(1);
    s.insert(4);
    s.insert(5);
    s.insert(2);
    s.insert(3);

    for(auto i = begin(s); i != end(s); i++) {
        std::cout << *i << ' ';
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
