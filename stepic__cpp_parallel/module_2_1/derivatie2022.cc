#include <iostream>

#include <list>
#include <forward_list>
#include <string>
#include <map>
#include <functional>


/** polynomial degree */
using degree = unsigned int;
/** polynomial factor */
using factor = int;

/** expression = factor * x ^ degree */


std::forward_list<std::string> explodeBy(const std::string &str, std::function<bool(char)>f)
{
    std::forward_list<std::string> res;
    std::string acc;
    for (const auto &c : str) {
        if (f(c)) {
            res.push_front(acc);
            acc.clear();
        }
        acc.push_back(c);
    }
    res.push_front(acc);
    return res;
}


/** take the derivate */
std::pair<degree,factor> derivate(const std::pair<degree,factor> &expr) {
    auto res = expr;
    res.second *= res.first;
    if (res.first > 0) res.first--;
    return res;
}


/** process one expression of polynomial */
std::pair<degree,factor> processExpression(const std::string &expr)
{
    factor f = 0;
    degree d = 0;
    
    std::string acc;
    
    auto lst = explodeBy(expr, [](char c) { return c == '*' || c == '^'; });
    
    // start from end, because it is std::forward_list
    
    auto it = lst.begin();
    if (it->size() > 1 && (*it)[0] == '^') {
        // ommitting result and error code
        try {
            d = std::stoi(std::string(it->data()+1, it->data() + it->size()));
        }
        catch(...)  {}  
        it++;
    }
    
    if (it != lst.end() && (*it == "x" || *it == "*x")) {
        it++;
        if (d == 0) d = 1;
        f = 1;
    }
    else if (it != lst.end() && *it == "+x") {
        if (d == 0) d = 1;
        f = 1;
    }
    else if (it != lst.end() && *it == "-x") {
        if (d == 0) d = 1;
        f = -1;
    }
    
    if (it != lst.end()) {
        auto start = it->data();
        if (*start == '+')
            start++;
        try {
            f = std::stoi(std::string(start, it->data() + it->size()));
        }
        catch(...) {}  
    }
    
    return std::make_pair(d,f);
}


std::string printMap(const std::map<degree, factor>&m) {
    std::string acc;
    
    for(auto it = m.rbegin(); it != m.rend(); it++) {
        if (it->second == 0) { // factor
            continue;
        }
        
        if (it->second >= 0) { // sign
            acc += "+";
        }
        if (it->second != 1 || it->first == 0) {
            acc += std::to_string(it->second);
        }
        if (it->first == 0)
            continue;
        if (it->first >= 1) {
            acc += "*x";
            if (it->first > 1)
                acc += "^" + std::to_string(it->first);
        }
    }
    if (acc.size() == 0 || acc == "+")
        acc = "0";
    if (acc.front() == '+')
        acc.erase(0, 1);
    // for(const auto &elem : m) {
    //     acc += std::to_string(elem.second) + "*x^" + std::to_string(elem.first) + " ";
    // }
    return acc;
}


/** process whole polynomial */
std::string processPolynomial(const std::string &poly) 
{   
    std::map<degree, factor> result;
    // go over string until first '+' or '-', skipping spaces
    std::string acc;

    auto lst = explodeBy(poly, [](char c) { return c == '-' || c == '+'; });
    
    for (const auto &elem : lst) {
        auto expr = derivate(processExpression(elem));
        auto prev = result.find(expr.first);
        if (prev != result.end()) {
            expr.second += prev->second;
        }
        result[expr.first] = expr.second;
        //result.insert();
    }
    
    return printMap(result);
}


// std::string derivative(std::string polynomial) {
//     return processPolynomial(polynomial);
// }


int main()
{
    std::list<std::string> lst = {
        "x", "-1*x", "x^", "x^2-x^2+x",
        "x^213+x", "2*x^100+100*x^2", "x^10000+x+1", 
        "-x^2-x^3", "x+x+x+x+x+x+x+x+x+x",
        
    };
    
    for(const auto &elem : lst) {
        std::cout << "main=" << elem << ' ' <<  processPolynomial(elem) << std::endl << std::endl;
    }

    return EXIT_SUCCESS;
}
