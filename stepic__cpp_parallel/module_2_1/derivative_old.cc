#include <list>
#include <map>


class CLexem {
public:
	bool minus;
	unsigned int a;
	unsigned int b;

	CLexem() : minus(false), a(1), b(0) {}

	int getA(void) const {
		return (minus == true) ? a * (-1) : a;
	}
	
	int getB(void) const {
		return b;
	}
	
	void addCLexemToMe(const CLexem& _lex) {
		int newA = getA() + _lex.getA();
		a = newA;
	}

	void derivativing(void) { // proizvodnaya
		a *= b;
		if (b > 0)
			b -= 1;
	}
	
	std::string to_string_sign_only_minus(void) {
		std::string str;
		if (a == 0)
			return str;
		if (minus == true)
			str += '-';
		return str;
	}
	
	std::string to_string_sign(void) {
		std::string str;
		if (a == 0)
			return str;
		if (minus == true)
			str += '-';
		else 
			str += '+';
		return str;
	}
	
	
	std::string to_string_without_sign(void) {
		std::string str;
		if (a == 0)
			return str;
		
		if (a != 0) {
			str += std::to_string(a);
		}
		if (b == 0)
			return str;
		
		str += '*';
		str += 'x';
		
		if (b != 1) {
			str += '^';
			str += std::to_string(b);
		}
		return str;
	}
	
	std::string to_string(void) {
		std::string str;
		if (a == 0)
			return str;
		
		if (minus == true)
			str += '-';
		
		if (a != 0) {
			str += std::to_string(a);
		}
		if (b == 0)
			return str;
		
		str += '*';
		str += 'x';
		
		if (b != 1) {
			str += '^';
			str += std::to_string(b);
		}
		return str;
	}
	
	
};


CLexem getLexem(std::string _str)
{
	CLexem lex;
	
	auto strbegin = begin(_str);
	auto strend = end(_str);
	auto it = strbegin;
	if (*it == '-') {
		lex.minus = true;
		it++;
	}
	if (*it == '+')
		it++;
	std::string valA;
	while ( (*it >= '0') && (*it <= '9') ) {
		valA.push_back(*it);
		it++;
	}
	if (valA.length() > 0) {  // is A
		lex.a = std::stoi(valA);
		if (*it != '*')
			return lex;
		else {
			it++;
			lex.b = 1;
			it++;
			if (*it != '^')
				return lex;
			it++;
			std::string valB;
			while ( (*it >= '0') && (*it <= '9') ) {
				valB.push_back(*it);
				it++;
			}
			if (valB.length() > 0)
				lex.b = std::stoi(valB);
			return lex;
		}
	}
	else  { // no A
		lex.a = 1;
		lex.b = 1;
		it++;
		if (*it != '^')
			return lex;
		it++;
		std::string valB;
		while ( (*it >= '0') && (*it <= '9') ) {
			valB.push_back(*it);
			it++;
		}
		if (valB.length() > 0)
			lex.b = std::stoi(valB);
		return lex;
	}
}

std::list<std::string> split_by_sign(std::string _str)
{	
	std::list<std::string> result;
	auto strbegin = begin(_str);
	auto strend = end(_str);
	auto it = strbegin;
	while (it != strend) {
		std::string current_string;
		current_string += *it;
		it++;
		while ( (it != strend) && (*it != '-') && (*it != '+') ) {
			current_string += *it;
			it++;
		}
		result.push_back(current_string);
	}
	return result;
}


std::string derivative(std::string polynomial) {
    	std::string result_str;
	
    // list of lexem's strings
	std::list<std::string> list_of_lexems_str = split_by_sign(polynomial);
	
	// map of lexem's objects
	std::multimap<int, CLexem> map_of_lexem;
	std::list<CLexem> result_list_of_lexems;
	
	for (auto lstitem: list_of_lexems_str) {
		CLexem lexem = getLexem(lstitem);
		lexem.derivativing();
		map_of_lexem.emplace(lexem.b,lexem);
	}

	
	auto m_it = begin(map_of_lexem);
	auto s_it = m_it;
	
    for (;  m_it != end(map_of_lexem);  m_it = s_it)
    {
        int theKey = (*m_it).first;
		CLexem firstLexemInSumm = (*m_it).second;
		auto keyRange = map_of_lexem.equal_range(theKey);
		s_it = keyRange.first;
		for (s_it++ ;  s_it != keyRange.second;  ++s_it)
        {
			firstLexemInSumm.addCLexemToMe((*s_it).second);
        }
		result_list_of_lexems.push_back(firstLexemInSumm);
    }
	
	auto it = result_list_of_lexems.rbegin();
	result_str += (*it).to_string_sign_only_minus();
	result_str += (*it).to_string_without_sign();
	
	for (it++;  it != result_list_of_lexems.rend(); it++) {
		result_str += (*it).to_string_sign();
		result_str += (*it).to_string_without_sign();
	}
	
    return result_str;

}
