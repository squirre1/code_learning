#include <list>
#include <map>
#include <string>
#include <iostream>


class CLexem {
public:
	bool minus;
	unsigned int a;
	unsigned int b;



};


std::list<std::string> split_by_sign(std::string _str)
{
	std::list<std::string> res;
	
	auto it_begin = begin(_str);
	auto it_end = begin(_str);
	auto it = it_begin;
	
	std::string buffer;
	
	while (it != it_end) {
		
		if ( ((*it) == '+') || ((*it) == '-') ) {
			if ((*it) == '-')
				buffer.push_back(*it);
			if (buffer.size() > 0) {
				res.push_back(buffer);
			}
		}
		else {
			buffer.push_back(*it);
		}
		it++;
	}
	
	return res;
}


std::string derivative(std::string polynomial)
{
	std::list<std::string> lst = split_by_sign(polynomial);
	
	std::string tmp;
	
	for(const auto &elem : lst) {
		tmp.append();
	}
	
	return tmp;
}


int main(void)
{
	std::list<std::string> test_data = {
		"2*x^100+100*x^2",
		"x^2+x",
		"x^10000+x+1",
		"-x^2-x^3",
		"x+x+x+x+x+x+x+x+x+x"
	};
	
	for(const auto &elem : test_data) {
		std::cout << elem << std::endl
				  << derivative(elem) << std::endl << std::endl;
	}
	
	//std::cout << derivative("2*x^100+100*x^2") << std::endl;
	return 0;
}
