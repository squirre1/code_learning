

Сокет
таблица дескрипторов


int fd = socket()

# Привязка сокета к адресу (сервер)
bind(s, (struct sockaddr *)sa, sizeof(sa));

struct sockaddr_in sa;
sa.sin_family = AF_INET;
sa.sin_port = ntnos(12345);
sa.sin_addr.s_addr = ntonl(INADDR_LOOPBACK);


```
INADDR_LOOPBACK = 127.0.0.1
INADDR_ANY = 0.0.0.0

Для остальных
ip = inet_addr("10.0.0.1")
inet_pton(AF_INET, "10.0.0.1", &(sa.sin_addr));
```

struct sockaddr_un sa;
sa.sun_family = AF_UNIX;
strcpy(sa.sun_path, "/tmp/a.sock"); 
??? Это корректно?
??? Да, пока файл открыт, его не удалят
??? по крону из папки tmp

# Слушаем  (сервер)
listem(s, SOMAXCONN): // 

//             clients values
accept(socket, (struct sockaddr *) addr, size?);

# принимаем соединения
while (s1 = accept(s,0,0)) {
	// s1 - new socket 
}

size_t read(int fd, buffer, size_t count);
size_t write(int fd, const void *buffer, size_t count);
SIGPIPE - остановит выполнение программы если коннект оборвется


size_t recv(int fd, buffer, size_t count, int flags);
size_t send(int fd, const void *buffer, size_t count, int flags);
flags = MSG_NOSIGNAL - убирает сигнал SIGPIPE


# клиент вызывает connect(int fd, struct sockaddr *, size_t)

метод тройного рукопожатия:

клиент: 
Отправляет пакет с флагом SYN J - произвольное число J
(тут происходит accept)

сервер: 
Возвращает пакет с флагом SYN K, ACK J+1

клиент: 
Отправляет пакет с флагом ACK K+1

----

тут могут начаться сенд и ресв


# Закрытие соединения

shutdown(fd, SHUT_RDWR | SHUT_RD | SHUT_WR)
close (fd);

клиент:
FIN M

сервер:
ACN M+1

сервер:
FIN N

клиент:
ACN N+1


# UDP

сервер: socket, bind

клиент: socket

сервер: size_t recvfrom(int s, void *buffer, size_t len, int flags, struct sockaddr *from, socklen_t *fromlen);

клиент: size_t sendto(int s, const void *buffer, size_t len, int flags, const struct sockaddr *to, socklen_t tolen);

576 - гарантированный размер датаграммы



# изначальное все сокеты блокирующие

```c++
// So, we can bind 'accept' to 'select'
int set_nonblock(int fd)
{
	int flags;
#if defined(O_NONBLOCK)
	if (-1 == (flags = fcntl(fd, F_GETFL, 0)))
		flags = 0;
	return fcntl(fd, F_SETFL, flags | O_NONBLOCK);
#else
	flags = 1;
	return ioctl(fd, FIOBIO, &flags);
#endif
}
```

# опции

после убития приложения, можно сразу его переиспользовать
optval = 1;
setsockopt(int fd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval);

таймаут чтения в блокирующем режиме
struct timeval tv;
tv.sec = 16; tv.usec = 0;
setsockout(int fd, SOL_SOCKET, SO_RCVTIMEO, (char *) &tv, sizeof(tv);

таймаут записи в блокирующем режиме
... SO_SNDTIMEO ...



# список дескрипторов

lsof -p 16648


# 

strace ./server

либо

sudo strace -p 16807
sudo ltrace -p 16807

sudo gdb -p 16807
(c,n,bt, p)
