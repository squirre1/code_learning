cmake_minimum_required(VERSION 3.9)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

project(module72)

set(SOURCES
  # main.c
# cv.cpp
cv2.cpp
)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pedantic -Wall -Werror")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic -Wall -Werror")


add_executable(module72 ${SOURCES})


target_link_libraries(module72 pthread)


add_custom_command(
  OUTPUT module72-output
  COMMAND ./module72
)


add_custom_target(
  Just_Run_Program ALL
  DEPENDS module72-output module72
)
