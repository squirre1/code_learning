#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>


void *thread_function(void *value)
{
    printf("thread_function\n");
    int *int_value = (int*) value;
    (*int_value)++;
    return value;
    /* return ; */
}

int main()
{
    pthread_t id;
    int val = 3;
    int* retval;
    pthread_create(&id, NULL, thread_function, &val);
    pthread_join(id, (void *)&retval);
    printf("pthread_join %d %d\n", val, *retval);
    return EXIT_SUCCESS;
}
